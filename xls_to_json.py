import csv, json, xlrd

def xlsParse(xlsfile):
    store = []  #List of objects to be parsed into json file
    framenames = []
    book = xlrd.open_workbook(xlsfile)
    sh1 = book.sheet_by_index(0)
    for rx in range(0, sh1.nrows):
        if sh1.row(rx)[1].value[0:2] not in framenames:
            # import pdb;pdb.set_trace()
            #Appending only unique value
            framenames.append(sh1.row(rx)[1].value[0:2])
            frame = {"state": sh1.row(rx)[1].value[0:2],
               "rto_code": []
               }
            store.append(frame)
        

    segment = {"segmentName":""}
    for frame in store:
        for rx in range(0, sh1.nrows):
            if frame["state"] == sh1.row(rx)[1].value[0:2]:
                segment = {
                "zone":sh1.row(rx)[0].value,
                "rto_code": sh1.row(rx)[1].value
                }
                frame["rto_code"].append(segment)
                #Appending the rto code

    # Parse the XLS into JSON
    out = json.dumps(store, indent=4)
    # Save the JSON
    f = open( 'xlsdata.json', 'w')
    f.write(out)

xlsParse('dataset.xls')